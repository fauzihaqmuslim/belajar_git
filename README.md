## UPLOAD PERUBAHAN

### lokal

1. git add .
2. git commit -m "pesan"

### repository pribadi

3. git push origin [branch yang diinginkan]

### repository utama

4. buat pull request pada halaman repo di bitbucket

## UPDATE REPO

### tambah remote link

1. git remote add [nama][link repo]

### cek remote

2. git remote -v

### update repo

3. git fetch [nama link repo utama][nama branch repo utama]

## BUAT BRANCH BARU

1. git branch [nama branch]

## HAPUS BRANCH

1. git branch -D [nama branch]
